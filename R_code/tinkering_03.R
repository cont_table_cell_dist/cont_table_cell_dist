for (coo in 1:n) {
    coo_path <- paste("./data/coo_", sprintf("%02d", coo), "/", sep="")
    Mixvol_name <- paste(coo_path, "mixvol_normed.txt", sep="")
    Mixvol <- as.matrix(read.table(Mixvol_name))
    nslice <- nrow(Mixvol)
    cat("coo =", coo, " num mixvol before last:", length(which(Mixvol[nslice-1,] > 0)), "\n")
    cat("first-last: (", length(which(Mixvol[1,] > 0)), ",", length(which(Mixvol[nslice,] > 0)), ")\n")
}

