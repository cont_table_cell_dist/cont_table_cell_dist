\documentclass{beamer}

\usepackage{amsthm,amsmath,amssymb}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{natbib}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{hyperref}
\newcommand{\theHalgorithm}{\arabic{algorithm}}

\usetheme{default}
\title{Marginal cell distribution in contingency tables with fixed row/column marginals}
\author[B. Haas, E. Airoldi]{Bertrand Haas \& Edoardo Airoldi}
\institute{Harvard University, Department of Statistics}
\date[Aug. 2012]{Aug. 23, 2012}

\begin{document}
\begin{frame}[plain]
\titlepage
\end{frame}

\begin{frame}{Contingency Tables}
\textsl{Contingency table} = cross classified table of counts where units are counted according to several categorical variables (like sex, age, nucleotides, genes, species).

Example:
\begin{center}
\includegraphics[width=0.9\textwidth]{contab_4x4_exple}
\end{center}
Multiway tables when number of variables $ > 2 $.\\
Interest:  (in)dependence of variables (R and C here).\\
Tools:  Fisher Exact test, Chi-squared, G-test, ...\\
Roughly:  Assuming independence, how extreme is that table among all tables with the same row/column marginals.
\end{frame}

\begin{frame}{Fisher exact test}
Mostly used for $ 2 $ by $ 2 $ tables with relatively small entries:
\begin{center}
\includegraphics[width=0.9\textwidth]{contab_2x2_gal}
\end{center}
Probability of getting the table is:
\[
P_0 = \frac{\binom{a+b}{a} \binom{c+d}{c}}{\binom{n}{a+c}} = \frac{(a+b)!(c+d)!(a+c)!(b+d)!}{a!b!c!d!n!}
\]
Compute the probability for \textsl{all} tables with same marginal distributions and add the probabilities that are at least as small as $ P_0 $ to get the p-value.
\end{frame}

\begin{frame}{Fisher approximate exact test}
For $ m \times n $ tables:
\[
P_0 = \frac{(R_1! R_2! \dots R_m!)(C_1! C_2! \dots C_n!)}{N!\prod_{i,j} x_{i,j}!}
\]
Theoretically that still works, but computationally... Cannot compute the probability for \textsl{all} the tables with fixed marginals.\\
Solution:
\begin{itemize}
\item Sample uniformly a manageably large number of tables.
\item Rescale the probabilities $ P_i \mapsto P'_i $ so they are not too small.
\item The p-value is $ p = \frac{\sum P'_j}{\sum P'_i} $ where the numerator is over the $ P'_j \leq P'_0 $ and the denominator is over all the $ P'_i $ in our sample.
\end{itemize}
New problem:  "How to efficiently sample tables form the set of all tables with fixed row/column marginals"
\end{frame}

\begin{frame}{The space of contingency tables with fixed marginals}
Re-state the constraints of fixed margins as a linear system $ Ax = b $ where $ x $ is the vectorized table, $ b $ is the vector of row and column marginals, and $ A $ is the incidence matrix.
If necessary disregard some rows to make $ A $ full rank $ m \times n $ matrix ($ m < n $).
In the $ 4 \times 4 $ table for example:
\[
\tiny
A = \left(\begin{array}{cccccccccccccccc}
1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    &    &    &    &    \\
&    &    &    & 1  & 1  & 1  & 1  &    &    &    &    &    &    &    &    \\
&    &    &    &    &    &    &    & 1  & 1  & 1  & 1  &    &    &    &    \\
&    &    &    &    &    &    &    &    &    &    &    & 1  & 1  & 1  & 1  \\
1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    \\
& 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    &    \\
&    & 1  &    &    &    & 1  &    &    &    & 1  &    &    &    & 1  &    
\end{array}\right)
\]
The system $ Ax = b, x \geq 0 $ defines a polytope $ \mathcal{P}^{(d)} $ of dimension $ d = n - m $.
We are interested in integer samples $ x $, but integer problems are usually more difficult than in $ \mathbb{R} $, so we first tackle the problem of sampling real points and we can see later how to round them appropraiately.
\end{frame}

\begin{frame}{Coordinate by coordinate sampling}
If we know the marginal distribution along a coordinate, we can sample a coordinate.
Holding this coordinate fixed, we get a section of $ \mathcal{P}^{(d)} $, another polytope $ \mathcal{P}^{(d-1)} $.
So we can iterate.
\begin{center}
\includegraphics[width=0.9\textwidth]{3d_ptope_sampling.png}
\end{center}
\end{frame}

\begin{frame}{Sections along the coordinate line}
Let's get the $ 1^\mathrm{st} $ coordinate marginal distribution.\\
Find the first coordinates $ t_0, t_1, \dots, t_N $ of all the vertices of $ \mathcal{P} $.\\
Above each $ s $ there is a section $ S(s) $.
Above $ s = t_i $ we write $ S_i $.
\begin{center}
\includegraphics[width=0.5\textwidth]{section_change.png}
\end{center}
\end{frame}

\begin{frame}{section volume in a slice}
The marginal pdf $ f(t) $ along the first coordinate (from the uniform pdf on $ \mathcal{P}) $) is proportional to the volume of the section:  $ f(t) \propto \mathrm{Vol}_{d-1}(S(t)) $. \\
The \textsl{slice} of $ \mathcal{P} $ between $ S_{i-1} $ and $ S_i $ is the convex hull of $ S_{i-1} $ and $ S_i $.\\
For $ t_{i-1} \leq t \leq t_i $ we have $ S(s) = sS_i + (1-s) S_{i-1} $ (with $ s = \frac{t- t{i-1}}{t_1 - t_{i-1}} $).\\
So $ \mathrm{Vol}_{d-1}(S(s)) = \mathrm{Vol}_{d-1}(sS_i + (1-s) S_{i-1}) $.
This is a homogeneous polynomial in two variables $ x = s $ and $ y = (1-s) $ of degree $ d-1 $.
\end{frame}

\begin{frame}{Minkowsky sums and mixed volumes}
Let $ A $ and $ B $ be $ 2 $ convex sets.
The \textsl{Minkowsky sum} $ A+B = \{a + b : a \in A, b \in B\} $.\\
Example:
\begin{center}
\includegraphics[width=0.6\textwidth]{Mink_sum.png}
\end{center}
Given convex sets $ A_1, \dots A_k $ of dimension $ n $, the volume of $ \lambda_1 A_1 + \lambda_2 A_2 + \dots + \lambda_k A_k $ is a homogeneous polynomial in $ \lambda_1, \lambda_2, \dots, \lambda_k $ of degree $ n $.\\
The coefficients of the monomial $ \lambda_{i1}\lambda_{i2} \dots \lambda_{in} $ is defined as the mixed volume $ V(A_{i1}, A_{i2}, \dots, A_{ik}) $.
\end{frame}

\begin{frame}{Section volumes as Bezier curves}
Now we can write $ \mathrm{Vol}_{d-1}(S(s)) $ as a homogeneous polynomial $ B(s) $:
\[
B(s) = \sum_{a,b \geq 0}^{a+b=d-1}  \binom{d-1}{a} V(S_1^a, S_0^b) s^a(1-s)^b
\]
Where $ V(A^a, B^b) $ is a shortcut for $ V(A, \dots, A, B, \dots, B) $ ($a$ times for $ A $ and $ b $ times for $ B $).\\
Such a polynomial is called a \textsl{Bernstein} polynomial and defines a \textsl{Bezier} curve with control points $ (a, V(S_1^a, S_0^b)) $.
\begin{center}
\includegraphics[width=0.6\textwidth]{Bezier_3_big.png}
\end{center}
\end{frame}

\begin{frame}{Section volumes as mixture of betas}
Rewrite
\[
B(s) = \sum_{a,b \geq 0}^{a+b = d-1} \frac{V(S_1^a, S_0^b)}{d}\left(\frac{\Gamma(d+1)}{\Gamma(a+1)\Gamma(b+1)} s^a(1-s)^b \right)
\]
Integrating $ B(s) $ between $ 0 $ and $ 1 $ yields the volume of the slice of $ \mathcal{P} $ between $ S_0 $ and $ S_1 $:
\[
\mathrm{Vol}_d(\mathcal{P}_{01}) = \frac{\sum_{a,b \geq 0}^{a+b=d-1} V(S_1^a, S_0^b)}{d}
\]
Which gives the scaling factor:  $ f(t) = \frac{1}{\mathrm{Vol}_d(\mathcal{P}_{i-1,i})} B(t) $ for $ t_{i-1} \leq t \leq t_i $
\end{frame}

\end{document}
